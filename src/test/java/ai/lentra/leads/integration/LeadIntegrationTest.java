package ai.lentra.leads.integration;

import ai.lentra.leads.dto.*;
import ai.lentra.leads.repository.LeadRepository;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.lang3.RandomUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.io.UnsupportedEncodingException;
import java.math.BigDecimal;
import java.util.Collections;
import java.util.List;

import static ai.lentra.leads.integration.TestUtil.createLead;
import static org.hamcrest.Matchers.is;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.patch;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * @author laxmik@lentra.ai
 */
@DirtiesContext
@AutoConfigureMockMvc
public class LeadIntegrationTest extends TestContainerSupport {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private LeadRepository leadRepository;

    ObjectMapper mapper = new ObjectMapper();

    public static final String LEAD_NOT_FOUND = "Lead/s not found";
    public static final String UPDATE_CONTACT_NOT_SUPPORTED =
            "PATCH /leads api does not support updating contacts";

    private static final String LEAD_CURRENT_STAGE = RandomStringUtils.randomAlphanumeric(15);
    private static final BigDecimal REQUESTED_LOAN_AMOUNT = BigDecimal.valueOf(2000000);
    private static final String PHONE_NUMBER = RandomStringUtils.randomNumeric(10);
    private static final String EMAIL = "sec_user@domain.com";
    private static final int SOURCING_USER_ID = RandomUtils.nextInt();
    private static LeadRequestDTO leadDtoJsonData;

    @BeforeAll
    static void readLeadJsonData() {
        leadDtoJsonData = TestUtil.readLeadDtoJson();
    }

    @AfterEach
    public void cleanUp() {
        leadRepository.deleteAll();
    }

    @Test
    public void test_createLeadWithAllDetails() throws Exception {
        MvcResult result = createLead(mockMvc, leadDtoJsonData);
        long leadId = getLeadId(result);
        String res = "{ \n\"leadId\":" + leadId + "\n }";
        ResponseEntity<?> expResponse = new ResponseEntity<>(res, HttpStatus.CREATED);
        assertEquals(expResponse.getBody(), result.getResponse().getContentAsString());
    }

    @Test
    public void test_getLead() throws Exception {
        MvcResult result = createLead(mockMvc, leadDtoJsonData);
        long leadId = getLeadId(result);

        mockMvc
                .perform(get("/leads/" + leadId).contentType(APPLICATION_JSON_VALUE))
                .andDo(print())
                .andExpect(content().contentType(APPLICATION_JSON_VALUE))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.current_stage").value("Document verification"))
                .andExpect(jsonPath("$.current_status").value("In Progress"))
                .andExpect(jsonPath("$.follow_up_date").value("2021-09-09"));
    }

    @Test
    public void test_createLeadWithPhoneNumber() throws Exception {
        PhoneDTO phone = new PhoneDTO();
        phone.setPhoneNumber(PHONE_NUMBER);
        ContactDTO contact = new ContactDTO();
        contact.setPhones(Collections.singletonList(phone));

        SourcingDetailsDTO sourcingDetails = new SourcingDetailsDTO();
        sourcingDetails.setUserId(SOURCING_USER_ID);

        LeadRequestDTO lead = new LeadRequestDTO();
        lead.setContacts(Collections.singletonList(contact));
        lead.setSourcingDetails(sourcingDetails);

        MvcResult result = createLead(mockMvc, lead);
        long leadId = getLeadId(result);

        // get created lead and assert values
        mockMvc
                .perform(get("/leads/" + leadId).contentType(APPLICATION_JSON_VALUE))
                .andDo(print())
                .andExpect(content().contentType(APPLICATION_JSON_VALUE))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.contacts[0].phones[0].phone_number").value(PHONE_NUMBER))
                .andExpect(jsonPath("$.sourcing_details.user_id").value(SOURCING_USER_ID))
                .andExpect(jsonPath("$.contacts[0].emails[0].email_id").doesNotExist())
                .andExpect(jsonPath("$.current_stage").doesNotExist())
                .andExpect(jsonPath("$.current_status").doesNotExist())
                .andExpect(jsonPath("$.follow_up_date").doesNotExist());
    }

    @Test
    public void test_createLeadWithEmail() throws Exception {
        EmailDTO email = new EmailDTO();
        email.setEmailId(EMAIL);
        ContactDTO contact = new ContactDTO();
        contact.setEmails(Collections.singletonList(email));

        SourcingDetailsDTO sourcingDetails = new SourcingDetailsDTO();
        sourcingDetails.setUserId(SOURCING_USER_ID);

        LeadRequestDTO lead = new LeadRequestDTO();
        lead.setContacts(Collections.singletonList(contact));
        lead.setSourcingDetails(sourcingDetails);

        MvcResult result = createLead(mockMvc, lead);
        long leadId = getLeadId(result);

        // get created lead and assert values
        mockMvc
                .perform(get("/leads/" + leadId).contentType(APPLICATION_JSON_VALUE))
                .andDo(print())
                .andExpect(content().contentType(APPLICATION_JSON_VALUE))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.contacts[0].emails[0].email_id").value(EMAIL))
                .andExpect(jsonPath("$.sourcing_details.user_id").value(SOURCING_USER_ID))
                .andExpect(jsonPath("$.contacts[0].phones[0].phone_number").doesNotExist())
                .andExpect(jsonPath("$.current_stage").doesNotExist())
                .andExpect(jsonPath("$.current_status").doesNotExist())
                .andExpect(jsonPath("$.follow_up_date").doesNotExist());
    }

    @Test
    public void test_createLeadWithReqLoanAmount() throws Exception {
        RequestedLoanDetailsDTO loanDetails = new RequestedLoanDetailsDTO();
        loanDetails.setAmount(REQUESTED_LOAN_AMOUNT);

        SourcingDetailsDTO sourcingDetails = new SourcingDetailsDTO();
        sourcingDetails.setUserId(SOURCING_USER_ID);

        LeadRequestDTO lead = new LeadRequestDTO();
        lead.setRequestedLoanDetails(loanDetails);
        lead.setSourcingDetails(sourcingDetails);

        MvcResult result = createLead(mockMvc, lead);
        long leadId = getLeadId(result);

        // get created lead and assert values
        mockMvc
                .perform(get("/leads/" + leadId).contentType(APPLICATION_JSON_VALUE))
                .andDo(print())
                .andExpect(content().contentType(APPLICATION_JSON_VALUE))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.requested_loan_details.loan_amount").value(REQUESTED_LOAN_AMOUNT))
                .andExpect(jsonPath("$.sourcing_details.user_id").value(SOURCING_USER_ID))
                .andExpect(jsonPath("$.contacts").doesNotExist())
                .andExpect(jsonPath("$.current_stage").doesNotExist())
                .andExpect(jsonPath("$.current_status").doesNotExist())
                .andExpect(jsonPath("$.follow_up_date").doesNotExist());
    }

    @Test
    public void test_createLeadWithSourcingDetails() throws Exception {
        SourcingDetailsDTO sourcingDetails = new SourcingDetailsDTO();
        sourcingDetails.setUserId(SOURCING_USER_ID);

        LeadRequestDTO lead = new LeadRequestDTO();
        lead.setSourcingDetails(sourcingDetails);

        MvcResult result = createLead(mockMvc, lead);
        long leadId = getLeadId(result);

        // get created lead and assert values
        mockMvc
                .perform(get("/leads/" + leadId).contentType(APPLICATION_JSON_VALUE))
                .andDo(print())
                .andExpect(content().contentType(APPLICATION_JSON_VALUE))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.sourcing_details.user_id").value(SOURCING_USER_ID))
                .andExpect(jsonPath("$.contacts").doesNotExist())
                .andExpect(jsonPath("$.current_stage").doesNotExist())
                .andExpect(jsonPath("$.current_status").doesNotExist())
                .andExpect(jsonPath("$.follow_up_date").doesNotExist());
    }

    @Test
    public void test_updateRequestedLoanDetails() throws Exception {
        MvcResult result = createLead(mockMvc, leadDtoJsonData);
        long leadId = getLeadId(result);

        mockMvc
                .perform(get("/leads/" + leadId).contentType(APPLICATION_JSON_VALUE))
                .andDo(print())
                .andExpect(content().contentType(APPLICATION_JSON_VALUE))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.requested_loan_details.loan_amount").value(new BigDecimal(1000000)));

        LeadRequestDTO updatedLead = updateReqLoanAmount(leadId);

        updateLead(updatedLead, leadId);

        mockMvc
                .perform(get("/leads/" + leadId).contentType(APPLICATION_JSON_VALUE))
                .andDo(print())
                .andExpect(content().contentType(APPLICATION_JSON_VALUE))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.requested_loan_details.loan_amount").value(REQUESTED_LOAN_AMOUNT));
    }

    @Test
    public void test_updateSourcingDetails() throws Exception {
        MvcResult result = createLead(mockMvc, leadDtoJsonData);
        long leadId = getLeadId(result);

        MvcResult getResult =
                mockMvc
                        .perform(get("/leads/" + leadId).contentType(APPLICATION_JSON_VALUE))
                        .andDo(print())
                        .andExpect(content().contentType(APPLICATION_JSON_VALUE))
                        .andExpect(status().isOk())
                        .andExpect(jsonPath("$.sourcing_details.user_id").value(1))
                        .andReturn();
        JSONObject sourcingDetails =
                new JSONObject(getResult.getResponse().getContentAsString())
                        .getJSONObject("sourcing_details");

        LeadRequestDTO updatedLead = updateSourcingDetails(leadId);
        updatedLead.getSourcingDetails().setId(sourcingDetails.getLong("id"));

        updateLead(updatedLead, leadId);

        mockMvc
                .perform(get("/leads/" + leadId).contentType(APPLICATION_JSON_VALUE))
                .andDo(print())
                .andExpect(content().contentType(APPLICATION_JSON_VALUE))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.sourcing_details.user_id").value(SOURCING_USER_ID));
    }

    @Test
    public void test_updateLeadsActivity() throws Exception {
        MvcResult result = createLead(mockMvc, leadDtoJsonData);
        long leadId = getLeadId(result);

        MvcResult getResult =
                mockMvc
                        .perform(get("/leads/" + leadId).contentType(APPLICATION_JSON_VALUE))
                        .andDo(print())
                        .andExpect(content().contentType(APPLICATION_JSON_VALUE))
                        .andExpect(status().isOk())
                        .andExpect(jsonPath("$.leads_activity.size()").value(1))
                        .andExpect(jsonPath("$.leads_activity[0].data").value("In progress"))
                        .andReturn();
        JSONArray array =
                new JSONObject(getResult.getResponse().getContentAsString()).getJSONArray("leads_activity");
        long activityId = array.getJSONObject(0).getLong("id");

        LeadRequestDTO updatedLead = updateLeadsActivity(leadId, activityId);

        updateLead(updatedLead, leadId);

        mockMvc
                .perform(get("/leads/" + leadId).contentType(APPLICATION_JSON_VALUE))
                .andDo(print())
                .andExpect(content().contentType(APPLICATION_JSON_VALUE))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.leads_activity.size()").value(2))
                .andExpect(jsonPath("$.leads_activity[0].data").value("Completed"))
                .andExpect(jsonPath("$.leads_activity[1].data").value("New Application"));
    }

    @Test
    public void test_updateLeadWithReqLoanAmountOnly() throws Exception {
        RequestedLoanDetailsDTO loanDetails = new RequestedLoanDetailsDTO();
        loanDetails.setAmount(new BigDecimal(9000000));

        SourcingDetailsDTO sourcingDetails = new SourcingDetailsDTO();
        sourcingDetails.setUserId(SOURCING_USER_ID);

        LeadRequestDTO lead = new LeadRequestDTO();
        lead.setRequestedLoanDetails(loanDetails);
        lead.setSourcingDetails(sourcingDetails);

        MvcResult result = createLead(mockMvc, lead);
        long leadId = getLeadId(result);

        mockMvc
                .perform(get("/leads/" + leadId).contentType(APPLICATION_JSON_VALUE))
                .andDo(print())
                .andExpect(content().contentType(APPLICATION_JSON_VALUE))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.requested_loan_details.loan_amount").value(new BigDecimal(9000000)))
                .andExpect(jsonPath("$.contacts").doesNotExist())
                .andExpect(jsonPath("$.current_stage").doesNotExist())
                .andExpect(jsonPath("$.current_status").doesNotExist())
                .andExpect(jsonPath("$.follow_up_date").doesNotExist());

        LeadRequestDTO updatedLead = updateReqLoanAmount(leadId);

        updateLead(updatedLead, leadId);

        mockMvc
                .perform(get("/leads/" + leadId).contentType(APPLICATION_JSON_VALUE))
                .andDo(print())
                .andExpect(content().contentType(APPLICATION_JSON_VALUE))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.requested_loan_details.loan_amount").value(REQUESTED_LOAN_AMOUNT))
                .andExpect(jsonPath("$.contacts").doesNotExist())
                .andExpect(jsonPath("$.current_stage").doesNotExist())
                .andExpect(jsonPath("$.current_status").doesNotExist())
                .andExpect(jsonPath("$.follow_up_date").doesNotExist());
    }

    // Negative scenarios

    @Test
    public void test_ifNoSourcingDetailsProvided_createLeadFails() throws Exception {
        LeadRequestDTO leadDto = new LeadRequestDTO();
        leadDto.setCurrentStage(LEAD_CURRENT_STAGE);

        String content = mapper.writeValueAsString(leadDto);
        mockMvc
                .perform(
                        MockMvcRequestBuilders.request(HttpMethod.POST, "/leads/create-lead")
                                .content(content)
                                .contentType(APPLICATION_JSON_VALUE))
                .andDo(print())
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.message").value("Bad Request"))
                .andExpect(jsonPath("$.status").value(400))
                .andExpect(jsonPath("$.error_details[0]").value("Provide sourcing details."));
    }

    @Test
    public void test_patchLeadApiCannotUpdateContact() throws Exception {
        MvcResult result = createLead(mockMvc, leadDtoJsonData);
        long leadId = getLeadId(result);

        LeadRequestDTO updatedLead = updateSourcingDetails(leadId);
        updatedLead.setContacts(Collections.singletonList(new ContactDTO())); // add dummy contact

        mockMvc
                .perform(
                        patch("/leads/update-lead/" + leadId)
                                .content(mapper.writeValueAsString(updatedLead))
                                .contentType(APPLICATION_JSON_VALUE))
                .andDo(print())
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.message").value(HttpStatus.BAD_REQUEST.getReasonPhrase()))
                .andExpect(jsonPath("$.error_details[0]").value(UPDATE_CONTACT_NOT_SUPPORTED));
    }

    @Test
    public void test_getLeadFails_forNonExistingLeadId() throws Exception {
        mockMvc
                .perform(get("/leads/0").contentType(APPLICATION_JSON_VALUE))
                .andDo(print())
                .andExpect(content().contentType(APPLICATION_JSON_VALUE))
                .andExpect(status().isNotFound())
                .andExpect(jsonPath("$.message").value(HttpStatus.NOT_FOUND.getReasonPhrase()))
                .andExpect(jsonPath("$.error_details[0]").value(LEAD_NOT_FOUND + " for given Id 0"));
    }

    @Test
    public void test_createOrUpdateFails_forEmptyReqPayload() throws Exception {
        MvcResult postResult =
                mockMvc
                        .perform(
                                MockMvcRequestBuilders.request(HttpMethod.POST, "/leads/create-lead")
                                        .contentType(APPLICATION_JSON_VALUE))
                        .andDo(print())
                        .andExpect(status().isBadRequest())
                        .andReturn();
        JSONObject obj = new JSONObject(postResult.getResponse().getContentAsString());
        assertTrue(obj.getString("error_details").contains("Required request body is missing:"));

        MvcResult patchResult =
                mockMvc
                        .perform(
                                MockMvcRequestBuilders.request(HttpMethod.PATCH, "/leads/update-lead/" + anyLong())
                                        .contentType(APPLICATION_JSON_VALUE))
                        .andDo(print())
                        .andExpect(status().isBadRequest())
                        .andReturn();
        obj = new JSONObject(patchResult.getResponse().getContentAsString());
        assertTrue(obj.getString("error_details").contains("Required request body is missing:"));
    }

    private long getLeadId(MvcResult result) throws UnsupportedEncodingException, JSONException {
        JSONObject obj = new JSONObject(result.getResponse().getContentAsString());
        return obj.getLong("leadId");
    }

    private void updateLead(LeadRequestDTO updatedLead, long leadId) throws Exception {
        mockMvc
                .perform(
                        patch("/leads/update-lead/" + leadId)
                                .content(mapper.writeValueAsString(updatedLead))
                                .contentType(APPLICATION_JSON_VALUE))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.message", is("Lead entity updated successfully.")));
    }

    private LeadRequestDTO updateReqLoanAmount(long leadId) {
        RequestedLoanDetailsDTO loanDetailsDTO = new RequestedLoanDetailsDTO();
        loanDetailsDTO.setAmount(REQUESTED_LOAN_AMOUNT);
        LeadRequestDTO leadDto = new LeadRequestDTO();
        leadDto.setRequestedLoanDetails(loanDetailsDTO);
        return leadDto;
    }

    private LeadRequestDTO updateSourcingDetails(long leadId) {
        SourcingDetailsDTO sourcingDetails = new SourcingDetailsDTO();
        sourcingDetails.setUserId(SOURCING_USER_ID);
        LeadRequestDTO leadDto = new LeadRequestDTO();
        leadDto.setSourcingDetails(sourcingDetails);
        return leadDto;
    }

    private LeadRequestDTO updateLeadsActivity(long leadId, long activityId) {
        LeadsActivityDTO act1 = new LeadsActivityDTO();
        act1.setId(activityId);
        act1.setData("Completed");

        LeadsActivityDTO act2 = new LeadsActivityDTO();
        act2.setData("New Application");

        LeadRequestDTO leadDto = new LeadRequestDTO();
        leadDto.setLeadsActivities(List.of(act1, act2));
        return leadDto;
    }
}
