package ai.lentra.leads.integration;

import ai.lentra.leads.dto.ContactDTO;
import ai.lentra.leads.dto.LeadRequestDTO;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import org.springframework.http.HttpMethod;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;

import static ai.lentra.leads.commons.EndPointReferer.*;
import static ai.lentra.leads.commons.EndPointReferer.CONTACTS;
import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * @author laxmik@lentra.ai
 */
public class TestUtil {

    private static final String CONTACT_BASE_URL = LEADS + LEAD_ID + CONTACTS;

    public static LeadRequestDTO readLeadDtoJson() {
        try {
            File file = new File("src/test/resources/lead.json");
            String json = new String(Files.readAllBytes(file.toPath()));
            Gson gson = new Gson();
            return gson.fromJson(json, LeadRequestDTO.class);
        } catch (IOException ex) {
            throw new RuntimeException(ex);
        }
    }

    public static MvcResult createLead(MockMvc mockMvc, LeadRequestDTO leadDto) {
        ObjectMapper mapper = new ObjectMapper();
        try {
            String content = mapper.writeValueAsString(leadDto);
            return mockMvc
                    .perform(
                            MockMvcRequestBuilders.request(HttpMethod.POST, "/leads/create-lead")
                                    .content(content)
                                    .contentType(APPLICATION_JSON_VALUE))
                    .andDo(print())
                    .andExpect(status().isCreated())
                    .andReturn();
        } catch (Exception ex) {
            throw new RuntimeException("Exception in creating Lead.");
        }
    }

    public static MvcResult createContact(MockMvc mockMvc, long leadId, ContactDTO contactDTO) {
        ObjectMapper mapper = new ObjectMapper();
        try {
            String content = mapper.writeValueAsString(contactDTO);
            return mockMvc
                    .perform(
                            MockMvcRequestBuilders.request(
                                            HttpMethod.POST, CONTACT_BASE_URL + CREATE_CONTACT, leadId)
                                    .content(content)
                                    .contentType(APPLICATION_JSON_VALUE))
                    .andDo(print())
                    .andExpect(status().isCreated())
                    .andReturn();
        } catch (Exception ex) {
            throw new RuntimeException("Exception in creating Contact.");
        }
    }
}
