package ai.lentra.leads.integration;

import ai.lentra.leads.dto.*;
import ai.lentra.leads.model.Address;
import ai.lentra.leads.repository.ContactRepository;
import ai.lentra.leads.repository.LeadRepository;
import org.apache.commons.lang3.RandomStringUtils;
import org.checkerframework.checker.units.qual.A;
import org.json.JSONException;
import org.json.JSONObject;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.HttpMethod;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.testcontainers.shaded.com.fasterxml.jackson.databind.ObjectMapper;

import java.io.UnsupportedEncodingException;
import java.util.Collections;

import static ai.lentra.leads.commons.EndPointReferer.*;
import static ai.lentra.leads.integration.TestUtil.createLead;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

//@Transactional
@DirtiesContext
@AutoConfigureMockMvc
public class ContactIntegrationTest extends TestContainerSupport {

    @Autowired
    private MockMvc mockMvc;
    @Autowired
    private ContactRepository contactRepository;
    @Autowired
    private LeadRepository leadRepository;

    private static final String CONTACT_EXTERNAL_ID = RandomStringUtils.randomAlphanumeric(15);
    private static final String CONTACT_CATEGORY = RandomStringUtils.randomAlphanumeric(15);
    private static final String PHONE_NUMBER = RandomStringUtils.randomNumeric(10);
    private static final String EMAIL_ID = "sec_user@domain.com";
    private static final String CITY = RandomStringUtils.randomAlphabetic(5);
    private static final String STATE = RandomStringUtils.randomAlphabetic(5);
    private static final String COUNTRY = RandomStringUtils.randomAlphabetic(5);
    private static final String CONTACT_BASE_URL = LEADS + LEAD_ID + CONTACTS;

    private final ObjectMapper objectMapper = new ObjectMapper();
    private static LeadRequestDTO leadDtoJsonData;

    @BeforeAll
    public static void readLeadJsonData() {
        leadDtoJsonData = TestUtil.readLeadDtoJson();
    }

    @AfterEach
    public void cleanUp() {
        leadRepository.deleteAll();
        contactRepository.deleteAll();
    }

    @Test
    public void test_createContact() {
        MvcResult result = createLead(mockMvc, leadDtoJsonData);
        long leadId = getLeadId(result);
        TestUtil.createContact(mockMvc, leadId, stubContactDto());
    }

    @Test
    public void test_updateContact() throws Exception {
        MvcResult leadResult = createLead(mockMvc, leadDtoJsonData);
        long leadId = getLeadId(leadResult);

        TestUtil.createContact(mockMvc, leadId, stubContactDto());

        MvcResult result = mockMvc
                .perform(
                        MockMvcRequestBuilders.request(HttpMethod.GET, CONTACT_BASE_URL, leadId)
                                .contentType(APPLICATION_JSON_VALUE))
                .andDo(print())
                .andExpect(status().isOk())
                .andReturn();
    }

    /*@Test
    public void test_createAddressInGivenContact() {
        TestUtil.createContact(mockMvc, anyLong(), stubContactDto());

        MvcResult result = mockMvc
                .perform(
                        MockMvcRequestBuilders.request(HttpMethod.GET, CONTACT_BASE_URL)
                                .contentType(APPLICATION_JSON_VALUE))
                .andDo(print())
                .andExpect(status().isOk())
                .andReturn();

        Address newAddress = new Address();
        newAddress.setCity("Mumbai");
        newAddress.setState("MH");
        newAddress.setCountry("India");

        mockMvc
                .perform(
                        MockMvcRequestBuilders.request(
                                        HttpMethod.POST, CONTACT_BASE_URL + CREATE_CONTACT, leadId)
                                .content(content)
                                .contentType(APPLICATION_JSON_VALUE))
                .andDo(print())
                .andExpect(status().isCreated())
    }*/

    @Test
    public void test_updateAddress() {

    }

    @Test
    public void test_getAddress() {

    }

    @Test
    public void test_getAllAddress() {

    }


    private ContactDTO stubContactDto() {
        ContactDTO contactDTO = new ContactDTO();
        contactDTO.setExternalId(CONTACT_EXTERNAL_ID);
        contactDTO.setCategory(CONTACT_CATEGORY);

        PhoneDTO phoneDTO = new PhoneDTO();
        phoneDTO.setPhoneNumber(PHONE_NUMBER);

        EmailDTO emailDTO = new EmailDTO();
        emailDTO.setEmailId(EMAIL_ID);

        AddressDTO addressDTO = new AddressDTO();
        addressDTO.setCity(CITY);
        addressDTO.setState(STATE);
        addressDTO.setCountry(COUNTRY);

        contactDTO.setPhones(Collections.singletonList(phoneDTO));
        contactDTO.setEmails(Collections.singletonList(emailDTO));
        contactDTO.setAddresses(Collections.singletonList(addressDTO));
        return contactDTO;
    }

    private long getLeadId(MvcResult result)  {
        try {
            JSONObject obj = new JSONObject(result.getResponse().getContentAsString());
            return obj.getLong("leadId");
        } catch (UnsupportedEncodingException | JSONException ex) {
            throw new RuntimeException("Exception in extracting lead Id from json.");
        }
    }
}
