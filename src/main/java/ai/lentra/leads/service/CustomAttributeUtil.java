package ai.lentra.leads.service;

import ai.lentra.leads.commons.CustomAttribute;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Component
public class CustomAttributeUtil {

    public List<CustomAttribute> updateCustomAttributes(List<CustomAttribute> updatedAttr, List<CustomAttribute> existingAttr) {
        if (!updatedAttr.isEmpty()) {
            Map<String, String> idToValueMap = updatedAttr.stream()
                    .collect(Collectors.toMap(CustomAttribute::getAttributeId, CustomAttribute::getSerializedValue));
            existingAttr.forEach(
                    attr -> {
                        if (idToValueMap.containsKey(attr.getAttributeId())) {
                            attr.setSerializedValue(idToValueMap.get(attr.getAttributeId()));
                        }
                    });
        }
        return existingAttr;
    }
}
