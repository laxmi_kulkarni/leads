package ai.lentra.leads.service;


import ai.lentra.leads.dto.LeadRequestDTO;
import ai.lentra.leads.model.Lead;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 * @author laxmik@lentra.ai
 */
public interface LeadsService {

    long createLead(Lead lead);

    void updateLead(long leadId, Lead lead);

    Lead findById(long id);

    Page<Lead> findAll(Pageable pageable);
}
