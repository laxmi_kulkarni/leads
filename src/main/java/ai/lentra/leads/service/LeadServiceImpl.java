package ai.lentra.leads.service;

import ai.lentra.leads.model.Lead;
import ai.lentra.leads.model.LeadsActivity;
import ai.lentra.leads.model.RequestedLoanDetails;
import ai.lentra.leads.model.SourcingDetails;
import ai.lentra.leads.repository.ContactRepository;
import ai.lentra.leads.repository.LeadRepository;
import org.apache.commons.lang3.ObjectUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import javax.validation.ValidationException;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static ai.lentra.leads.commons.ErrorMessage.LEAD_NOT_FOUND;
import static org.apache.commons.lang3.ObjectUtils.firstNonNull;

/**
 * @author laxmik@lentra.ai
 */
@Service
public class LeadServiceImpl implements LeadsService {

    private static final Logger logger = LoggerFactory.getLogger(LeadServiceImpl.class);

    @Autowired
    private LeadRepository leadRepository;

    @Autowired
    private ContactRepository contactRepository;

    @Override
    public long createLead(Lead lead) {
        lead.setCreatedOn(LocalDateTime.now());
        return leadRepository.save(lead).getId();
    }

    @Override
    public void updateLead(long leadId, Lead lead) {
        Lead existingLead = getLead(leadId);
        updateLeadEntity(lead, existingLead);
        leadRepository.save(existingLead);
    }

    @Override
    public Lead findById(long id) {
        return getLead(id);
    }

    @Override
    public Page<Lead> findAll(Pageable pageable) {
        return leadRepository.findAll(pageable);
    }

    private Lead getLead(long id) {
        return leadRepository.findById(id).orElseThrow(() -> new EntityNotFoundException(LEAD_NOT_FOUND + " for given Id " + id));
    }

    private void updateLeadEntity(Lead updatedLead, Lead existingLead) {
        existingLead.setInstitutionId(firstNonNull(updatedLead.getInstitutionId(), existingLead.getInstitutionId()));
        existingLead.setCurrentStage(firstNonNull(updatedLead.getCurrentStage(), existingLead.getCurrentStage()));
        existingLead.setCurrentStatus(firstNonNull(updatedLead.getCurrentStatus(), existingLead.getCurrentStatus()));
        existingLead.setExternalId(firstNonNull(updatedLead.getExternalId(), existingLead.getExternalId()));
        existingLead.setExternalIdType(firstNonNull(updatedLead.getExternalIdType(), existingLead.getExternalIdType()));
        existingLead.setLastUpdatedOn(LocalDateTime.now());
        existingLead.setFollowUpDate(firstNonNull(updatedLead.getFollowUpDate(), existingLead.getFollowUpDate()));
        existingLead.setRequestedLoanDetails(updateReqLoanDetails(updatedLead.getRequestedLoanDetails(), existingLead.getRequestedLoanDetails()));
        existingLead.setSourcingDetails(updateSourcingDetails(updatedLead.getSourcingDetails(), existingLead.getSourcingDetails()));
        existingLead.setLeadsActivities(updateLeadsActivity(updatedLead.getLeadsActivities(), existingLead.getLeadsActivities()));
    }

    private List<LeadsActivity> updateLeadsActivity(List<LeadsActivity> updatedActivity, List<LeadsActivity> existingActivity) {
        if (updatedActivity != null) {
            Map<Long, LeadsActivity> idToActivityMap = updatedActivity.stream().filter(a -> a.getId() != null)
                    .collect(Collectors.toMap(LeadsActivity::getId, a -> a));

            existingActivity.forEach(activity -> {
                if (idToActivityMap.containsKey(activity.getId())) {
                    activity.setData(firstNonNull(idToActivityMap.get(activity.getId()).getData(), activity.getData()));
                    activity.setType(firstNonNull(idToActivityMap.get(activity.getId()).getType(), activity.getType()));
                }
            });
            existingActivity.addAll(addIfNewActivity(updatedActivity));
        }
        return existingActivity;
    }

    List<LeadsActivity> addIfNewActivity(List<LeadsActivity> activities) {
        return activities.stream().filter(activity -> activity.getId() == null).collect(Collectors.toList());
    }

    private SourcingDetails updateSourcingDetails(SourcingDetails updatedSourceDetails, SourcingDetails existingSourceDetails) {
        if (updatedSourceDetails != null) {
            if (updatedSourceDetails.getId() == null) {
                throw new ValidationException("Provide Sourcing details Id to update.");
            }
            existingSourceDetails.setBranch(firstNonNull(updatedSourceDetails.getBranch(), existingSourceDetails.getBranch()));
            existingSourceDetails.setLocation(firstNonNull(updatedSourceDetails.getLocation(), existingSourceDetails.getLocation()));
            existingSourceDetails.setUserId(firstNonNull(updatedSourceDetails.getUserId(), existingSourceDetails.getUserId()));
            existingSourceDetails.setChannelId(firstNonNull(updatedSourceDetails.getChannelId(), existingSourceDetails.getChannelId()));
            existingSourceDetails.setChannelType(firstNonNull(updatedSourceDetails.getChannelType(), existingSourceDetails.getChannelType()));
            existingSourceDetails.setMedium(firstNonNull(updatedSourceDetails.getMedium(), existingSourceDetails.getMedium()));
        }
        return existingSourceDetails;
    }

    private RequestedLoanDetails updateReqLoanDetails(RequestedLoanDetails updatedLoanDetails, RequestedLoanDetails existingLoanDetails) {
        if (updatedLoanDetails != null) {
            existingLoanDetails.setAmount(firstNonNull(updatedLoanDetails.getAmount(), existingLoanDetails.getAmount()));
            existingLoanDetails.setPayment(firstNonNull(updatedLoanDetails.getPayment(), existingLoanDetails.getPayment()));
            existingLoanDetails.setTenure(firstNonNull(updatedLoanDetails.getTenure(), existingLoanDetails.getTenure()));
            existingLoanDetails.setPaymentFrequency(firstNonNull(updatedLoanDetails.getPaymentFrequency(), existingLoanDetails.getPaymentFrequency()));
            existingLoanDetails.setTenureUnit(firstNonNull(updatedLoanDetails.getTenureUnit(), existingLoanDetails.getTenureUnit()));
        }
        return existingLoanDetails;
    }
}
