package ai.lentra.leads.service;

import ai.lentra.leads.model.Address;
import ai.lentra.leads.model.Contact;
import ai.lentra.leads.model.Email;
import ai.lentra.leads.model.Phone;

import java.util.List;

public interface ContactService {

    void createContact(Long leadId, Contact contact);

    void updateContact(Long leadId, Long contactId, Contact contact);

    Contact findContactById(Long leadId, Long contactId);

    List<Contact> findAllContacts(Long leadId);

    //Address services
    void createAddress(Long leadId, Long contactId, Address address);

    void updateAddress(Long leadId, Long contactId, Long addressId, Address address);

    Address findAddressById(Long leadId, Long contactId, Long addressId);

    List<Address> findAllAddresses(Long leadId, Long contactId);

    //Phone services
    void createPhoneDetails(Long leadId, Long contactId, Phone phone);

    void updatePhoneDetails(Long leadId, Long contactId, Long phoneId, Phone phone);

    Phone findPhoneById(Long leadId, Long contactId, Long phoneId);

    List<Phone> findAllPhoneDetails(Long leadId, Long contactId);

    //Email services
    void createEmail(Long leadId, Long contactId, Email email);

    void updateEmailDetails(Long leadId, Long contactId, Long emailId, Email email);

    Email findEmailById(Long leadId, Long contactId, Long emailId);

    List<Email> findAllEmails(Long leadId, Long contactId);

}
