package ai.lentra.leads.service;

import ai.lentra.leads.model.*;
import ai.lentra.leads.repository.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import java.util.List;
import java.util.Objects;

import static ai.lentra.leads.commons.ErrorMessage.*;
import static org.apache.commons.lang3.ObjectUtils.firstNonNull;

@Service
public class ContactServiceImpl implements ContactService {

    private static final Logger logger = LoggerFactory.getLogger(ContactServiceImpl.class);

    @Autowired
    private ContactRepository contactRepository;
    @Autowired
    private LeadRepository leadRepository;
    @Autowired
    private AddressRepository addressRepository;
    @Autowired
    private PhoneRepository phoneRepository;
    @Autowired
    private EmailRepository emailRepository;
    @Autowired
    private LeadsService leadsService;
    @Autowired
    private CustomAttributeUtil customAttributeUtil;

    @Override
    public void createContact(Long leadId, Contact contact) {
        leadsService.findById(leadId); // check lead exist
        Contact newContact = contactRepository.save(contact);
        contactRepository.saveLeadContactAssociation(leadId, newContact.getId());
    }

    @Override
    public void updateContact(Long leadId, Long contactId, Contact contact) {
        checkIfContactBelongsToGivenLead(leadId, contactId);
        Contact existingContact = getContact(contact.getId());
        updateContactEntity(contact, existingContact);
        contactRepository.save(existingContact);
    }

    @Override
    public Contact findContactById(Long leadId, Long contactId) {
        checkIfContactBelongsToGivenLead(leadId, contactId);
        return getContact(contactId);
    }

    @Override
    public List<Contact> findAllContacts(Long leadId) {
        Lead lead = leadsService.findById(leadId);
        return lead.getContacts();
    }

    @Override
    public void createAddress(Long leadId, Long contactId, Address address) {
        checkIfContactBelongsToGivenLead(leadId, contactId);
        Contact contact = getContact(contactId);
        contact.getAddresses().add(address);
        contactRepository.save(contact);
    }

    @Override
    public void updateAddress(Long leadId, Long contactId, Long addressId, Address address) {
        checkIfContactBelongsToGivenLead(leadId, contactId);
        Contact contact = contactRepository.getById(contactId);
        Address existingAdd = contact.getAddresses().stream()
                .filter(add -> Objects.equals(add.getId(), addressId))
                .findFirst()
                .orElseThrow(() -> new EntityNotFoundException(ADDRESS_NOT_FOUND_IN_CONTACT));
        updateAddressEntity(existingAdd, address);
        addressRepository.save(existingAdd);
    }

    @Override
    public Address findAddressById(Long leadId, Long contactId, Long addressId) {
        checkIfContactBelongsToGivenLead(leadId, contactId);
        Contact contact = getContact(contactId);
        return getAddress(contact, addressId);
    }

    @Override
    public List<Address> findAllAddresses(Long leadId, Long contactId) {
        checkIfContactBelongsToGivenLead(leadId, contactId);
        Contact contact = getContact(contactId);
        return contact.getAddresses();
    }

    @Override
    public void createPhoneDetails(Long leadId, Long contactId, Phone phone) {
        checkIfContactBelongsToGivenLead(leadId, contactId);
        Contact contact = getContact(contactId);
        contact.getPhones().add(phone);
        contactRepository.save(contact);
    }

    @Override
    public void updatePhoneDetails(Long leadId, Long contactId, Long phoneId, Phone phone) {
        checkIfContactBelongsToGivenLead(leadId, contactId);
        Contact contact = contactRepository.getById(contactId);
        Phone existingPhone = contact.getPhones().stream()
                .filter(p -> p.getId() == phoneId)
                .findFirst()
                .orElseThrow(() -> new EntityNotFoundException(PHONE_DETAILS_NOT_FOUND_IN_CONTACT));
        updatePhoneEntity(existingPhone, phone);
        phoneRepository.save(existingPhone);
    }

    @Override
    public Phone findPhoneById(Long leadId, Long contactId, Long phoneId) {
        checkIfContactBelongsToGivenLead(leadId, contactId);
        Contact contact = getContact(contactId);
        return getPhone(contact, phoneId);
    }

    @Override
    public List<Phone> findAllPhoneDetails(Long leadId, Long contactId) {
        checkIfContactBelongsToGivenLead(leadId, contactId);
        Contact contact = getContact(contactId);
        return contact.getPhones();
    }

    @Override
    public void createEmail(Long leadId, Long contactId, Email email) {
        checkIfContactBelongsToGivenLead(leadId, contactId);
        Contact contact = getContact(contactId);
        contact.getEmails().add(email);
        contactRepository.save(contact);
    }

    @Override
    public void updateEmailDetails(Long leadId, Long contactId, Long emailId, Email email) {
        checkIfContactBelongsToGivenLead(leadId, contactId);
        Contact contact = contactRepository.getById(contactId);
        Email existingEmail = contact.getEmails().stream()
                .filter(e -> Objects.equals(e.getId(), emailId))
                .findFirst()
                .orElseThrow(() -> new EntityNotFoundException(EMAIL_NOT_FOUND_IN_CONTACT));
        updateEmailEntity(existingEmail, email);
        emailRepository.save(existingEmail);
    }

    @Override
    public Email findEmailById(Long leadId, Long contactId, Long emailId) {
        checkIfContactBelongsToGivenLead(leadId, contactId);
        Contact contact = getContact(contactId);
        return getEmail(contact, emailId);
    }

    @Override
    public List<Email> findAllEmails(Long leadId, Long contactId) {
        checkIfContactBelongsToGivenLead(leadId, contactId);
        Contact contact = getContact(contactId);
        return contact.getEmails();
    }

    private Contact getContact(Long id) {
        return contactRepository
                .findById(id)
                .orElseThrow(() -> new EntityNotFoundException(CONTACT_NOT_FOUND + " for given Id " + id));
    }

    private Address getAddress(Contact contact, Long addressId) {
        return contact.getAddresses().stream().filter(addr -> Objects.equals(addr.getId(), addressId))
                .findFirst()
                .orElseThrow(() -> new EntityNotFoundException(ADDRESS_NOT_FOUND_IN_CONTACT));
    }

    private Phone getPhone(Contact contact, Long phoneId) {
        return contact.getPhones().stream().filter(phone -> Objects.equals(phone.getId(), phoneId))
                .findFirst()
                .orElseThrow(() -> new EntityNotFoundException(PHONE_DETAILS_NOT_FOUND_IN_CONTACT));
    }

    private Email getEmail(Contact contact, Long emailId) {
        return contact.getEmails().stream().filter(email -> Objects.equals(email.getId(), emailId))
                .findFirst()
                .orElseThrow(() -> new EntityNotFoundException(EMAIL_NOT_FOUND_IN_CONTACT));
    }

    private void checkIfContactBelongsToGivenLead(Long leadId, Long contactId) {
        Long id = contactRepository.findLeadContactAssociation(leadId, contactId);
        if (id == null) {
            throw new EntityNotFoundException(CONTACT_NOT_FOUND_IN_LEAD);
        }
   /* boolean contactExistForGivenLead =
            lead.getContacts().stream().anyMatch(contact -> contact.getId().equals(contactId));
    if (!contactExistForGivenLead) {
      throw new EntityNotFoundException(CONTACT_NOT_FOUND_IN_LEAD);
    }*/
    }

    private void updateContactEntity(Contact updatedContact, Contact existingContact) {
        updateIdentityDetails(updatedContact, existingContact);
        existingContact.setDateOfBirth(firstNonNull(updatedContact.getDateOfBirth(), existingContact.getDateOfBirth()));
        existingContact.setGender(firstNonNull(updatedContact.getGender(), existingContact.getGender()));
        existingContact.setMaritalStatus(firstNonNull(updatedContact.getMaritalStatus(), existingContact.getMaritalStatus()));
        existingContact.setReligion(firstNonNull(updatedContact.getReligion(), existingContact.getReligion()));
        existingContact.setNationality(firstNonNull(updatedContact.getNationality(), existingContact.getNationality()));
        existingContact.setDesignation(firstNonNull(updatedContact.getDesignation(), existingContact.getDesignation()));
        existingContact.setType(firstNonNull(updatedContact.getType(), existingContact.getType()));
        existingContact.setCategory(firstNonNull(updatedContact.getCategory(), existingContact.getCategory()));
        existingContact.setExternalId(firstNonNull(updatedContact.getExternalId(), existingContact.getExternalId()));
        existingContact.setExternalIdType(firstNonNull(updatedContact.getExternalIdType(), existingContact.getExternalIdType()));
        customAttributeUtil.updateCustomAttributes(updatedContact.getCustomAttributes(), existingContact.getCustomAttributes());
    }

    private void updateIdentityDetails(Contact updatedContact, Contact existingContact) {
        existingContact.setFirstName(firstNonNull(updatedContact.getFirstName(), existingContact.getFirstName()));
        existingContact.setMiddleName(firstNonNull(updatedContact.getMiddleName(), existingContact.getMiddleName()));
        existingContact.setLastName(firstNonNull(updatedContact.getLastName(), existingContact.getLastName()));
        existingContact.setMotherName(firstNonNull(updatedContact.getMotherName(), existingContact.getMotherName()));
        existingContact.setFatherName(firstNonNull(updatedContact.getFatherName(), existingContact.getFatherName()));
    }

    private void updateAddressEntity(Address updatedAddress, Address existingAddress) {
        existingAddress.setAddressType(firstNonNull(updatedAddress.getAddressType(), existingAddress.getAddressType()));
        existingAddress.setAddressLine1(firstNonNull(updatedAddress.getAddressLine1(), existingAddress.getAddressLine1()));
        existingAddress.setAddressLine2(firstNonNull(updatedAddress.getAddressLine2(), existingAddress.getAddressLine2()));
        existingAddress.setAddressLine3(firstNonNull(updatedAddress.getAddressLine3(), existingAddress.getAddressLine3()));
        existingAddress.setAddressLine4(firstNonNull(updatedAddress.getAddressLine4(), existingAddress.getAddressLine4()));
        existingAddress.setCity(firstNonNull(updatedAddress.getCity(), existingAddress.getCity()));
        existingAddress.setState(firstNonNull(updatedAddress.getState(), existingAddress.getState()));
        existingAddress.setCountry(firstNonNull(updatedAddress.getCountry(), existingAddress.getCountry()));
        existingAddress.setPincode(firstNonNull(updatedAddress.getPincode(), existingAddress.getPincode()));
        existingAddress.setLatitude(firstNonNull(updatedAddress.getLatitude(), existingAddress.getLatitude()));
        existingAddress.setLongitude(firstNonNull(updatedAddress.getLongitude(), existingAddress.getLongitude()));
        existingAddress.setMailDeliveryStatus(firstNonNull(updatedAddress.isMailDeliveryStatus(), existingAddress.isMailDeliveryStatus()));
        existingAddress.setAddressProofId(firstNonNull(updatedAddress.getAddressProofId(), existingAddress.getAddressProofId()));
        customAttributeUtil.updateCustomAttributes(updatedAddress.getCustomAttributes(), existingAddress.getCustomAttributes());
    }

    private void updatePhoneEntity(Phone updatedPhone, Phone existingPhone) {
        existingPhone.setPhoneNumber(firstNonNull(updatedPhone.getPhoneNumber(), existingPhone.getPhoneNumber()));
        existingPhone.setCountryCode(firstNonNull(updatedPhone.getCountryCode(), existingPhone.getCountryCode()));
        existingPhone.setProvinceCode(firstNonNull(updatedPhone.getProvinceCode(), existingPhone.getProvinceCode()));
        existingPhone.setCommunicationLocationType(firstNonNull(updatedPhone.getCommunicationLocationType(), existingPhone.getCommunicationLocationType()));
        existingPhone.setCommunicationType(firstNonNull(updatedPhone.getCommunicationType(), existingPhone.getCommunicationType()));
        existingPhone.setCommunicationVerification(firstNonNull(updatedPhone.getCommunicationVerification(), existingPhone.getCommunicationVerification()));
        existingPhone.setAlternatePhoneNumer(firstNonNull(updatedPhone.getAlternatePhoneNumer(), existingPhone.getAlternatePhoneNumer()));
    }

    private void updateEmailEntity(Email updatedEmail, Email existingEmail) {
        existingEmail.setEmailId(firstNonNull(updatedEmail.getEmailId(), existingEmail.getEmailId()));
        existingEmail.setEmailIdOfGuarantor(firstNonNull(updatedEmail.getEmailIdOfGuarantor(), existingEmail.getEmailIdOfGuarantor()));
        existingEmail.setEmailVerificationType(firstNonNull(updatedEmail.getEmailVerificationType(), existingEmail.getEmailVerificationType()));
    }

}
