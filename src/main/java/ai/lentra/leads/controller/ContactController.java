package ai.lentra.leads.controller;

import ai.lentra.leads.commons.EndPointReferer;
import ai.lentra.leads.commons.Mapper;
import ai.lentra.leads.commons.SuccessResponse;
import ai.lentra.leads.dto.AddressDTO;
import ai.lentra.leads.dto.ContactDTO;
import ai.lentra.leads.dto.EmailDTO;
import ai.lentra.leads.dto.PhoneDTO;
import ai.lentra.leads.model.Address;
import ai.lentra.leads.model.Contact;
import ai.lentra.leads.model.Email;
import ai.lentra.leads.model.Phone;
import ai.lentra.leads.service.ContactService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.mapstruct.factory.Mappers;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.persistence.EntityNotFoundException;
import java.util.List;

import static ai.lentra.leads.commons.EndPointReferer.*;
import static ai.lentra.leads.commons.ErrorMessage.*;

@RestController
@RequestMapping(
        value = EndPointReferer.LEADS + LEAD_ID + CONTACTS,
        produces = MediaType.APPLICATION_JSON_VALUE,
        consumes = MediaType.APPLICATION_JSON_VALUE)
@Tag(name = "Contact APIs")
public class ContactController {

    private static final Logger logger = LoggerFactory.getLogger(ContactController.class);
    private static final Mapper mapper = Mappers.getMapper(Mapper.class);
    @Autowired
    private ContactService contactService;

    @Operation(summary = "Add contact to given lead")
    @PostMapping(CREATE_CONTACT)
    public ResponseEntity<?> createContact(@RequestBody ContactDTO contactDTO, @PathVariable Long leadId) {
        logger.info("Started API request for Creating contact...");
        Contact contact = mapper.contactDtoToEntity(contactDTO);
        contactService.createContact(leadId, contact);
        logger.info("Done Creating contact...");
        SuccessResponse successResponse = new SuccessResponse("Contact added successfully.", null);
        return new ResponseEntity<>(successResponse, HttpStatus.CREATED);
    }

    @Operation(summary = "Update contact")
    @PatchMapping(UPDATE_CONTACT + CONTACT_ID)
    public ResponseEntity<?> updateContact(@RequestBody ContactDTO contactDTO, @PathVariable Long leadId, @PathVariable Long contactId) {
        logger.info("Started API request for Updating contact with contactId {}...", contactId);
        Contact contact = mapper.contactDtoToEntity(contactDTO);
        contactService.updateContact(leadId, contactId, contact);
        logger.info("Done Updating contact with contact Id {} ...", contactId);
        SuccessResponse successResponse = new SuccessResponse("Contact updated successfully.", null);
        return new ResponseEntity<>(successResponse, HttpStatus.OK);
    }

    @Operation(summary = "Get Single Contact")
    @GetMapping(CONTACT_ID)
    public ResponseEntity<ContactDTO> getContact(@PathVariable Long leadId, @PathVariable Long contactId) {
        logger.info("Started API call to get contact details for Id {} ...", contactId);
        ContactDTO contactDTO = mapper.contactEntityToDto(contactService.findContactById(leadId, contactId));
        logger.info("Done getting contact details with response {}...", contactDTO.toString());
        return new ResponseEntity<>(contactDTO, HttpStatus.OK);
    }

    @Operation(summary = "Get all contacts")
    @GetMapping()
    public ResponseEntity<?> getAllContacts(@PathVariable Long leadId) {
        List<Contact> contacts = contactService.findAllContacts(leadId);
        if (!contacts.isEmpty()) {
            return ResponseEntity.status(HttpStatus.OK).body(contacts);
        }
        throw new EntityNotFoundException(CONTACT_NOT_FOUND);
    }

    @Operation(summary = "Add Address to given Contact")
    @PostMapping(CONTACT_ID + CREATE_ADDRESS)
    public ResponseEntity<?> createAddress(@RequestBody AddressDTO addressDTO, @PathVariable Long leadId, @PathVariable Long contactId) {
        logger.info("Started API request for Creating Address...");
        Address address = mapper.addressDtoToEntity(addressDTO);
        contactService.createAddress(leadId, contactId, address);
        logger.info("Done Creating Address...");
        SuccessResponse successResponse = new SuccessResponse("Address added successfully to given contact.", null);
        return new ResponseEntity<>(successResponse, HttpStatus.CREATED);
    }

    @Operation(summary = "Update Address")
    @PatchMapping(CONTACT_ID + UPDATE_ADDRESS + ADDRESS_ID)
    public ResponseEntity<?> updateAddress(@RequestBody AddressDTO addressDTO, @PathVariable Long leadId,
                                           @PathVariable Long contactId, @PathVariable Long addressId) {
        logger.info("Started API request for Updating address with addressId {}...", addressId);
        Address address = mapper.addressDtoToEntity(addressDTO);
        contactService.updateAddress(leadId, contactId, addressId, address);
        logger.info("Done Updating address with address Id {} ...", addressId);
        SuccessResponse successResponse = new SuccessResponse("Address updated successfully.", null);
        return new ResponseEntity<>(successResponse, HttpStatus.OK);
    }

    @Operation(summary = "Get Single Address")
    @GetMapping(CONTACT_ID + ADDRESS + ADDRESS_ID)
    public ResponseEntity<AddressDTO> getAddress(@PathVariable Long leadId, @PathVariable Long contactId, @PathVariable Long addressId) {
        logger.info("Started API call to get address details for Id {} ...", addressId);
        AddressDTO addressDTO = mapper.addressEntityToDto(contactService.findAddressById(leadId, contactId, addressId));
        logger.info("Done getting address details with response {}...", addressDTO.toString());
        return new ResponseEntity<>(addressDTO, HttpStatus.OK);
    }

    @Operation(summary = "Get all Addresses")
    @GetMapping(CONTACT_ID + ADDRESS)
    public ResponseEntity<?> getAllAddresses(@PathVariable Long leadId, @PathVariable Long contactId) {
        List<Address> addresses = contactService.findAllAddresses(leadId, contactId);
        if (!addresses.isEmpty()) {
            return ResponseEntity.status(HttpStatus.OK).body(addresses);
        }
        throw new EntityNotFoundException(ADDRESS_NOT_FOUND);
    }

    @Operation(summary = "Add Phone detail to given Contact")
    @PostMapping(CONTACT_ID + CREATE_PHONE)
    public ResponseEntity<?> createPhone(@RequestBody PhoneDTO phoneDTO, @PathVariable Long leadId, @PathVariable Long contactId) {
        logger.info("Started API request for Creating Phone...");
        Phone phone = mapper.phoneDtoToEntity(phoneDTO);
        contactService.createPhoneDetails(leadId, contactId, phone);
        logger.info("Done Creating Phone details...");
        SuccessResponse successResponse = new SuccessResponse("Phone details added successfully to given contact.", null);
        return new ResponseEntity<>(successResponse, HttpStatus.CREATED);
    }

    @Operation(summary = "Update Phone detail")
    @PatchMapping(CONTACT_ID + UPDATE_PHONE + PHONE_ID)
    public ResponseEntity<?> updatePhone(@RequestBody PhoneDTO phoneDTO, @PathVariable Long leadId,
                                         @PathVariable Long contactId, @PathVariable Long phoneId) {
        logger.info("Started API request for Updating phone with phone Id {}...", phoneId);
        Phone phone = mapper.phoneDtoToEntity(phoneDTO);
        contactService.updatePhoneDetails(leadId, contactId, phoneId, phone);
        logger.info("Done Updating phone with phone Id {} ...", phoneId);
        SuccessResponse successResponse = new SuccessResponse("Phone details updated successfully.", null);
        return new ResponseEntity<>(successResponse, HttpStatus.OK);
    }

    @Operation(summary = "Get Single Phone detail")
    @GetMapping(CONTACT_ID + PHONES + PHONE_ID)
    public ResponseEntity<PhoneDTO> getPhone(@PathVariable Long leadId, @PathVariable Long contactId, @PathVariable Long phoneId) {
        logger.info("Started API call to get phone details for Id {} ...", phoneId);
        PhoneDTO phoneDTO = mapper.phoneEntityToDto(contactService.findPhoneById(leadId, contactId, phoneId));
        logger.info("Done getting phone details with response {}...", phoneDTO.toString());
        return new ResponseEntity<>(phoneDTO, HttpStatus.OK);
    }

    @Operation(summary = "Get all Phone details")
    @GetMapping(CONTACT_ID + PHONES)
    public ResponseEntity<?> getAllPhones(@PathVariable Long leadId, @PathVariable Long contactId) {
        List<Phone> phones = contactService.findAllPhoneDetails(leadId, contactId);
        if (!phones.isEmpty()) {
            return ResponseEntity.status(HttpStatus.OK).body(phones);
        }
        throw new EntityNotFoundException(PHONE_DETAILS_NOT_FOUND);
    }

    @Operation(summary = "Add Email to given Contact")
    @PostMapping(CONTACT_ID + CREATE_EMAIL)
    public ResponseEntity<?> createEmail(@RequestBody EmailDTO emailDTO, @PathVariable Long leadId, @PathVariable Long contactId) {
        logger.info("Started API request for Creating Email...");
        Email email = mapper.emailDtoToEntity(emailDTO);
        contactService.createEmail(leadId, contactId, email);
        logger.info("Done Creating Email...");
        SuccessResponse successResponse = new SuccessResponse("Email added successfully to given contact.", null);
        return new ResponseEntity<>(successResponse, HttpStatus.CREATED);
    }

    @Operation(summary = "Update Email")
    @PatchMapping(CONTACT_ID + UPDATE_EMAIL + EMAIL_ID)
    public ResponseEntity<?> updateEmail(@RequestBody EmailDTO emailDTO, @PathVariable Long leadId,
                                         @PathVariable Long contactId, @PathVariable Long emailId) {
        logger.info("Started API request for Updating email with emailId {}...", emailId);
        Email email = mapper.emailDtoToEntity(emailDTO);
        contactService.updateEmailDetails(leadId, contactId, emailId, email);
        logger.info("Done Updating email with email Id {} ...", emailId);
        SuccessResponse successResponse = new SuccessResponse("Email updated successfully.", null);
        return new ResponseEntity<>(successResponse, HttpStatus.OK);
    }

    @Operation(summary = "Get Single Email")
    @GetMapping(CONTACT_ID + EMAILS + EMAIL_ID)
    public ResponseEntity<EmailDTO> getEmail(@PathVariable Long leadId, @PathVariable Long contactId, @PathVariable Long emailId) {
        logger.info("Started API call to get email details for Id {} ...", emailId);
        EmailDTO emailDTO = mapper.emailEntityToDto(contactService.findEmailById(leadId, contactId, emailId));
        logger.info("Done getting email details with response {}...", emailDTO.toString());
        return new ResponseEntity<>(emailDTO, HttpStatus.OK);
    }

    @Operation(summary = "Get all Emails")
    @GetMapping(CONTACT_ID + EMAILS)
    public ResponseEntity<?> getAllEmails(@PathVariable Long leadId, @PathVariable Long contactId) {
        List<Email> emails = contactService.findAllEmails(leadId, contactId);
        if (!emails.isEmpty()) {
            return ResponseEntity.status(HttpStatus.OK).body(emails);
        }
        throw new EntityNotFoundException(EMAIL_NOT_FOUND);
    }
}
