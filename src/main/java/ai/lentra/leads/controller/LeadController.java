package ai.lentra.leads.controller;

import ai.lentra.leads.commons.*;
import ai.lentra.leads.dto.LeadRequestDTO;
import ai.lentra.leads.model.Lead;
import ai.lentra.leads.service.LeadsService;
import io.swagger.annotations.ApiParam;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.everit.json.schema.Schema;
import org.everit.json.schema.loader.SchemaLoader;
import org.json.JSONObject;
import org.json.JSONTokener;
import org.mapstruct.factory.Mappers;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PagedResourcesAssembler;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.PagedModel;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.persistence.EntityNotFoundException;

import static ai.lentra.leads.commons.EndPointReferer.*;
import static ai.lentra.leads.commons.ErrorMessage.LEAD_NOT_FOUND;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;

/**
 * @author laxmik@lentra.ai
 */
@RestController
@RequestMapping(
        value = EndPointReferer.LEADS,
        produces = MediaType.APPLICATION_JSON_VALUE,
        consumes = MediaType.APPLICATION_JSON_VALUE)
@Tag(name = "Lead APIs")
public class LeadController {

    @Autowired
    private LeadsService leadsService;

    private static final Mapper mapper = Mappers.getMapper(Mapper.class);

    private static final Logger logger = LoggerFactory.getLogger(LeadController.class);

    @Operation(summary = "Save Lead record")
    @PostMapping(CREATE_LEAD)
    public ResponseEntity<?> createLead(
            @Validated(CreateGrp.class) @RequestBody LeadRequestDTO leadDTO) {
        logger.info("Started API request for Creating lead...");
        Lead lead = mapper.leadDtoToEntity(leadDTO);
        long leadId = leadsService.createLead(lead);
        logger.info("Done Creating lead. Lead Id is {} ...", leadId);
        String res = "{ \n\"leadId\":" + leadId + "\n }";
        return new ResponseEntity<>(res, HttpStatus.CREATED);
    }

    @Operation(summary = "Update Lead record")
    @PatchMapping(UPDATE_LEAD + LEAD_ID)
    public ResponseEntity<?> updateLead(
            @Validated(UpdateGrp.class) @RequestBody LeadRequestDTO leadRequestDTO,
            @PathVariable long leadId) {
        logger.info("Started API request for Updating lead with leadId {}...", leadId);
        Lead lead = mapper.leadDtoToEntity(leadRequestDTO);
        leadsService.updateLead(leadId, lead);
        logger.info("Done Updating lead with lead Id {} ...", leadId);
        SuccessResponse successResponse =
                new SuccessResponse("Lead entity updated successfully.", null);
        return new ResponseEntity<>(successResponse, HttpStatus.OK);
    }

    @Operation(summary = "Get Single Lead record")
    @GetMapping(LEAD_ID)
    public ResponseEntity<?> getLeadById(@PathVariable long leadId) {
        logger.info("Started API call to get lead details for Id {} ...", leadId);
        LeadRequestDTO leadDTO = mapper.leadEntityToDto(leadsService.findById(leadId));
        logger.info("Done getting lead details with response {}...", leadDTO.toString());
        return new ResponseEntity<>(leadDTO, HttpStatus.OK);
    }

    @Operation(summary = "Get all leads")
    @GetMapping(consumes = MediaType.ALL_VALUE, produces = MediaType.ALL_VALUE)
    public ResponseEntity<?> getAllLeads(@ApiParam Pageable pageable, PagedResourcesAssembler<LeadRequestDTO> assembler) {
        logger.info("Started API call to get all leads ...");
        Page<Lead> leads = leadsService.findAll(pageable);
        if (leads.hasContent()) {
            Page<LeadRequestDTO> leadsDtoPage = leads.map(mapper::leadEntityToDto);
            PagedModel<EntityModel<LeadRequestDTO>> model = assembler.toModel(leadsDtoPage);
            HttpHeaders headers = new HttpHeaders();
            headers.setContentType(MediaType.valueOf(MediaType.APPLICATION_JSON_VALUE));
            return new ResponseEntity<>(model, headers, HttpStatus.OK);
        }
        throw new EntityNotFoundException(LEAD_NOT_FOUND);
    }

    private void validateLeadRequestInput(String leadRequest) {
        JSONObject jsonSchema =
                new JSONObject(
                        new JSONTokener(
                                LeadController.class.getResourceAsStream("/schema/leadrequest.schema.json")));
        JSONObject jsonObject = new JSONObject(leadRequest);
        Schema schema = SchemaLoader.load(jsonSchema);
        schema.validate(jsonObject);
    }
}
