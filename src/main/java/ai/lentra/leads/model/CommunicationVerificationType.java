package ai.lentra.leads.model;

/**
 * @author laxmik@lentra.ai
 */
public enum CommunicationVerificationType {

    OTP,
    MISSED_CALL,
    SMS
}
