package ai.lentra.leads.model;

import ai.lentra.leads.commons.CustomAttribute;
import ai.lentra.leads.commons.JsonDataUserType;
import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;

import javax.persistence.*;
import java.util.List;

/**
 * @author laxmik@lentra.ai
 */
@Entity
@Table(name = "SOURCING_DETAILS")
@TypeDef(name = "JsonDataUserType", typeClass = JsonDataUserType.class)
public class SourcingDetails {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(columnDefinition = "serial")
    private Long id;

    @Column(name = "CHANNEL_TYPE")
    private String channelType;

    @Column(name = "CHANNEL_ID")
    private Integer channelId;

    @Column(name = "MEDIUM")
    private String medium;

    @Column(name = "USER_ID")
    private Integer userId;

    @Column(name = "LOCATION")
    private String location;

    @Column(name = "BRANCH")
    private String branch;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "lead_id")
    @JsonBackReference
    private Lead lead;

    @Column(name = "CUSTOM_ATTRIBUTES")
    @Type(type = "JsonDataUserType")
    private List<CustomAttribute> customAttributes;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getChannelType() {
        return channelType;
    }

    public void setChannelType(String channelType) {
        this.channelType = channelType;
    }

    public Integer getChannelId() {
        return channelId;
    }

    public void setChannelId(Integer channelId) {
        this.channelId = channelId;
    }

    public String getMedium() {
        return medium;
    }

    public void setMedium(String medium) {
        this.medium = medium;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getBranch() {
        return branch;
    }

    public void setBranch(String branch) {
        this.branch = branch;
    }

    public Lead getLead() {
        return lead;
    }

    public void setLead(Lead lead) {
        this.lead = lead;
    }

    public List<CustomAttribute> getCustomAttributes() {
        return customAttributes;
    }

    public void setCustomAttributes(List<CustomAttribute> customAttributes) {
        this.customAttributes = customAttributes;
    }
}
