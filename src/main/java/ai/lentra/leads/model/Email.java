package ai.lentra.leads.model;

import ai.lentra.leads.commons.CustomAttribute;
import ai.lentra.leads.commons.JsonDataUserType;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;

import javax.persistence.*;
import java.util.List;

/**
 * @author laxmik@lentra.ai
 */
@Entity
@Table(name = "EMAIL")
@TypeDef(name = "JsonDataUserType", typeClass = JsonDataUserType.class)
public class Email {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(columnDefinition = "serial")
    private Long id;

    @Column(name = "EMAIL_ID")
    private String emailId;

    @Column(name = "EMAIL_OF_GUARANTOR")
    private String emailIdOfGuarantor;

    @Column(name = "EMAIL_VERIFICATION_TYPE")
    private EmailVerificationType emailVerificationType;

    @Column(name = "CUSTOM_ATTRIBUTES")
    @Type(type = "JsonDataUserType")
    private List<CustomAttribute> customAttributes;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getEmailId() {
        return emailId;
    }

    public void setEmailId(String emailId) {
        this.emailId = emailId;
    }

    public String getEmailIdOfGuarantor() {
        return emailIdOfGuarantor;
    }

    public void setEmailIdOfGuarantor(String emailIdOfGuarantor) {
        this.emailIdOfGuarantor = emailIdOfGuarantor;
    }

    public EmailVerificationType getEmailVerificationType() {
        return emailVerificationType;
    }

    public void setEmailVerificationType(EmailVerificationType emailVerificationType) {
        this.emailVerificationType = emailVerificationType;
    }

    public List<CustomAttribute> getCustomAttributes() {
        return customAttributes;
    }

    public void setCustomAttributes(List<CustomAttribute> customAttributes) {
        this.customAttributes = customAttributes;
    }
}
