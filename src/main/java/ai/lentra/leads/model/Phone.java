package ai.lentra.leads.model;

import ai.lentra.leads.commons.CustomAttribute;
import ai.lentra.leads.commons.JsonDataUserType;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;

import javax.persistence.*;
import java.util.List;

/**
 * @author laxmik@lentra.ai
 */
@Entity
@Table(name = "PHONE")
@TypeDef(name = "JsonDataUserType", typeClass = JsonDataUserType.class)
public class Phone {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(columnDefinition = "serial")
    private Long id;

    @Column(name = "PHONE_NUMBER")
    private String phoneNumber;

    @Column(name = "COUNTRY_CODE")
    private Integer countryCode;

    @Column(name = "PROVINCE_CODE")
    private Integer provinceCode;

    @Column(name = "COMMUNICATION_LOCATION_TYPE")
    private CommunicationLocationType communicationLocationType;

    @Column(name = "COMMUNICATION_TYPE")
    private CommunicationType communicationType;

    @Column(name = "COMMUNICATION_VERIFICATION")
    private CommunicationVerificationType communicationVerificationType;

    @Column(name = "ALTERNATE_PHONE_NUMBER")
    private String alternatePhoneNumer;

    @Column(name = "CUSTOM_ATTRIBUTES")
    @Type(type = "JsonDataUserType")
    private List<CustomAttribute> customAttributes;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public Integer getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(Integer countryCode) {
        this.countryCode = countryCode;
    }

    public Integer getProvinceCode() {
        return provinceCode;
    }

    public void setProvinceCode(Integer provinceCode) {
        this.provinceCode = provinceCode;
    }

    public CommunicationLocationType getCommunicationLocationType() {
        return communicationLocationType;
    }

    public void setCommunicationLocationType(CommunicationLocationType communicationLocationType) {
        this.communicationLocationType = communicationLocationType;
    }

    public CommunicationType getCommunicationType() {
        return communicationType;
    }

    public void setCommunicationType(CommunicationType communicationType) {
        this.communicationType = communicationType;
    }

    public CommunicationVerificationType getCommunicationVerification() {
        return communicationVerificationType;
    }

    public void setCommunicationVerification(CommunicationVerificationType communicationVerificationType) {
        this.communicationVerificationType = communicationVerificationType;
    }

    public String getAlternatePhoneNumer() {
        return alternatePhoneNumer;
    }

    public void setAlternatePhoneNumer(String alternatePhoneNumer) {
        this.alternatePhoneNumer = alternatePhoneNumer;
    }

    public List<CustomAttribute> getCustomAttributes() {
        return customAttributes;
    }

    public void setCustomAttributes(List<CustomAttribute> customAttributes) {
        this.customAttributes = customAttributes;
    }
}
