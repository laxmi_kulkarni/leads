package ai.lentra.leads.model;

/**
 * @author laxmik@lentra.ai
 */
public enum PaymentFrequency {
    DAILY,
    WEEKLY,
    MONTHLY,
    QUARTERLY,
    ANNUALLY;
}
