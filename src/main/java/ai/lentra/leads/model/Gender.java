package ai.lentra.leads.model;

/**
 * @author laxmik@lentra.ai
 */
public enum Gender {
    MALE,
    FEMALE,
    OTHER;
}
