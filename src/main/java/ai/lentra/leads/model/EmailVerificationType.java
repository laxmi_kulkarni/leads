package ai.lentra.leads.model;

/**
 * @author laxmik@lentra.ai
 */
public enum EmailVerificationType {
    VERIFICATION_LINK;
}
