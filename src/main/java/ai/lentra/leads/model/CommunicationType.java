package ai.lentra.leads.model;

/**
 * @author laxmik@lentra.ai
 */
public enum CommunicationType {
    MOBILE,
    LANDLINE
}
