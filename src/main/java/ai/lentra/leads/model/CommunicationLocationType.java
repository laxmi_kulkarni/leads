package ai.lentra.leads.model;

/**
 * @author laxmik@lentra.ai
 */
public enum CommunicationLocationType {

    HOME,
    OFFICE
}
