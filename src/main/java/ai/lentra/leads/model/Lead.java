package ai.lentra.leads.model;

import ai.lentra.leads.commons.CustomAttribute;
import ai.lentra.leads.commons.JsonDataUserType;
import ai.lentra.leads.commons.UpdateGrp;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

/**
 * @author laxmik@lentra.ai
 */
@Entity
@Table(name = "LEAD")
@TypeDef(name = "JsonDataUserType", typeClass = JsonDataUserType.class)
public class Lead {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    @Column
    private Long id;

    @Column(name = "INSTITUTION_ID")
    private String institutionId;

    @Column(name = "CURRENT_STAGE")
    private String currentStage; // master ref

    @Column(name = "CURRENT_STATUS")
    private String currentStatus; //master ref

    @Column(name = "EXTERNAL_ID")
    private String externalId;

    @Column(name = "EXTERNAL_ID_TYPE")
    private Integer externalIdType;

    @Column(name = "CREATED_ON", columnDefinition = "DATE")
    private LocalDateTime createdOn;

    @Column(name = "LAST_UPDATED_ON", columnDefinition = "DATE")
    private LocalDateTime lastUpdatedOn;

    @Column(name = "FOLLOW_UP_DATE", columnDefinition = "DATE")
    private LocalDate followUpDate;

    @Embedded
    private RequestedLoanDetails requestedLoanDetails;

    @OneToOne(mappedBy = "lead", cascade = CascadeType.ALL)
    @JsonManagedReference
    private SourcingDetails sourcingDetails;

    @OneToMany(cascade = CascadeType.ALL, targetEntity = Contact.class, fetch = FetchType.LAZY)
    @JoinTable(name = "LEAD_CONTACT",
            joinColumns = @JoinColumn(name = "lead_id"),
            inverseJoinColumns = @JoinColumn(name = "contact_id"),
            uniqueConstraints = {@UniqueConstraint(columnNames = {"lead_id", "contact_id"})})
    private List<Contact> contacts;

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    @JoinColumn(name = "leads_id")
    private List<LeadsActivity> leadsActivities;

    @Column(name = "CUSTOM_ATTRIBUTES")
    @Type(type = "JsonDataUserType")
    private List<CustomAttribute> customAttributes;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getInstitutionId() {
        return institutionId;
    }

    public void setInstitutionId(String institutionId) {
        this.institutionId = institutionId;
    }

    public String getCurrentStage() {
        return currentStage;
    }

    public void setCurrentStage(String currentStage) {
        this.currentStage = currentStage;
    }

    public String getCurrentStatus() {
        return currentStatus;
    }

    public void setCurrentStatus(String currentStatus) {
        this.currentStatus = currentStatus;
    }

    public List<Contact> getContacts() {
        return contacts;
    }

    public String getExternalId() {
        return externalId;
    }

    public void setExternalId(String externalId) {
        this.externalId = externalId;
    }

    public Integer getExternalIdType() {
        return externalIdType;
    }

    public void setExternalIdType(Integer externalIdType) {
        this.externalIdType = externalIdType;
    }

    public LocalDateTime getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(LocalDateTime createdOn) {
        this.createdOn = createdOn;
    }

    public LocalDateTime getLastUpdatedOn() {
        return lastUpdatedOn;
    }

    public void setLastUpdatedOn(LocalDateTime lastUpdatedOn) {
        this.lastUpdatedOn = lastUpdatedOn;
    }

    public LocalDate getFollowUpDate() {
        return followUpDate;
    }

    public void setFollowUpDate(LocalDate followUpDate) {
        this.followUpDate = followUpDate;
    }

    public RequestedLoanDetails getRequestedLoanDetails() {
        return requestedLoanDetails;
    }

    public void setRequestedLoanDetails(RequestedLoanDetails requestedLoanDetails) {
        this.requestedLoanDetails = requestedLoanDetails;
    }

    public SourcingDetails getSourcingDetails() {
        return sourcingDetails;
    }

    public void setSourcingDetails(SourcingDetails sourcingDetails) {
        if (sourcingDetails == null) {
            if (this.sourcingDetails != null) {
                this.sourcingDetails.setLead(null);
            }
        } else {
            sourcingDetails.setLead(this);
        }
        this.sourcingDetails = sourcingDetails;
    }

    public void setContacts(List<Contact> contacts) {
        this.contacts = contacts;
    }

    public List<LeadsActivity> getLeadsActivities() {
        return leadsActivities;
    }

    public void setLeadsActivities(List<LeadsActivity> leadsActivities) {
        this.leadsActivities = leadsActivities;
    }

    public List<CustomAttribute> getCustomAttributes() {
        return customAttributes;
    }

    public void setCustomAttributes(List<CustomAttribute> customAttributes) {
        this.customAttributes = customAttributes;
    }
}
