package ai.lentra.leads.model;

import javax.persistence.*;
import java.math.BigDecimal;

/**
 * @author laxmik@lentra.ai
 */

@Embeddable
public class RequestedLoanDetails {

    @Column(name = "AMOUNT")
    private BigDecimal amount;

    @Column(name = "PAYMENT")
    private BigDecimal payment;

    @Column(name = "PAYMENT_FREQUENCY")
    private String paymentFrequency;

    @Column(name = "TENURE")
    private Integer tenure;

    @Column(name = "TENURE_UNIT")
    private String tenureUnit;

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public BigDecimal getPayment() {
        return payment;
    }

    public void setPayment(BigDecimal payment) {
        this.payment = payment;
    }

    public String getPaymentFrequency() {
        return paymentFrequency;
    }

    public void setPaymentFrequency(String paymentFrequency) {
        this.paymentFrequency = paymentFrequency;
    }

    public Integer getTenure() {
        return tenure;
    }

    public void setTenure(Integer tenure) {
        this.tenure = tenure;
    }

    public String getTenureUnit() {
        return tenureUnit;
    }

    public void setTenureUnit(String tenureUnit) {
        this.tenureUnit = tenureUnit;
    }
}
