package ai.lentra.leads.model;

import ai.lentra.leads.commons.JsonDataUserType;
import org.hibernate.annotations.TypeDef;

import javax.persistence.*;

/**
 * @author laxmik@lentra.ai
 */
@Entity
@Table(name = "LEADS_ACTIVITY")
@TypeDef(name = "JsonDataUserType", typeClass = JsonDataUserType.class)
public class LeadsActivity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(columnDefinition = "serial")
    private Long id;

    @Column(name = "DATA")
    private String data;

    @Column(name = "type")
    private String type;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
