package ai.lentra.leads.model;

/**
 * @author laxmik@lentra.ai
 */
public enum MaritalStatus {
    SINGLE,
    MARRIED,
    DIVORCED;
}
