package ai.lentra.leads.model;

/**
 * @author laxmik@lentra.ai
 */
public enum AddressType {
    CURRENT_HOME,
    CURRENT_OFFICE,
    PERMANENT_HOME,
    PERMANENT_OFFICE;
}
