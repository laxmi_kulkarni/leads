package ai.lentra.leads.exception;

/**
 * @author laxmik@lentra.ai
 */

public class InvalidOperationException extends RuntimeException {

    public String message;

    public InvalidOperationException(String message) {
        super(message);
        this.message = message;
    }
}
