package ai.lentra.leads.exception;

import ai.lentra.leads.commons.ErrorResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.ServletWebRequest;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import javax.persistence.EntityNotFoundException;
import javax.validation.ValidationException;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author laxmik@lentra.ai
 */

@ControllerAdvice
public class GlobalExceptionHandler extends ResponseEntityExceptionHandler {

    private static final Logger logger = LoggerFactory.getLogger(GlobalExceptionHandler.class);

    @Override
    protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex,
                                                                  HttpHeaders headers, HttpStatus status, WebRequest req) {
        logger.error("Bad request exception thrown", ex);
        String requestURI = ((ServletWebRequest) req).getRequest().getRequestURI();
        List<String> errors = ex.getBindingResult().getAllErrors().stream().map(ObjectError::getDefaultMessage).collect(Collectors.toList());
        //return handleExceptionInternal(ex, buildAndReturnErrorResponse(errors, HttpStatus.BAD_REQUEST, requestURI), headers, HttpStatus.BAD_REQUEST, req);
        return buildAndReturnErrorResponse(errors, HttpStatus.BAD_REQUEST, requestURI);
    }

    @Override
    protected ResponseEntity<Object> handleHttpMessageNotReadable(HttpMessageNotReadableException ex, HttpHeaders headers,
                                                                  HttpStatus status, WebRequest req) {
        logger.error("Not readable request exception thrown", ex);
        String requestURI = ((ServletWebRequest) req).getRequest().getRequestURI();
        return buildAndReturnErrorResponse(Collections.singletonList(ex.getMessage()), HttpStatus.BAD_REQUEST, requestURI);
    }

    @ExceptionHandler({EntityNotFoundException.class})
    public ResponseEntity<?> handleEntityNotFoundEx(EntityNotFoundException ex, WebRequest req) {
        logger.error("Entity Not Found Exception thrown", ex);
        String requestURI = ((ServletWebRequest) req).getRequest().getRequestURI();
        return buildAndReturnErrorResponse(Collections.singletonList(ex.getMessage()), HttpStatus.NOT_FOUND, requestURI);
    }

    @ExceptionHandler({InvalidOperationException.class})
    public ResponseEntity<?> handleInvalidOpEx(InvalidOperationException ex, WebRequest req) {
        logger.error("Invalid Operation Exception thrown", ex);
        String requestURI = ((ServletWebRequest) req).getRequest().getRequestURI();
        return buildAndReturnErrorResponse(Collections.singletonList(ex.getMessage()), HttpStatus.BAD_REQUEST, requestURI);
    }

    @ExceptionHandler({ValidationException.class})
    public ResponseEntity<?> handleValidationEx(ValidationException ex, WebRequest req) {
        logger.error("Validation Exception thrown", ex);
        String requestURI = ((ServletWebRequest) req).getRequest().getRequestURI();
        return buildAndReturnErrorResponse(Collections.singletonList(ex.getMessage()), HttpStatus.BAD_REQUEST, requestURI);
    }

    @ExceptionHandler({Exception.class, RuntimeException.class})
    public ResponseEntity<?> handleGenericEx(Exception ex, WebRequest req) {
        logger.error("Generic Exception thrown", ex);
        String requestURI = ((ServletWebRequest) req).getRequest().getRequestURI();
        return buildAndReturnErrorResponse(Collections.singletonList(ex.getMessage()), HttpStatus.INTERNAL_SERVER_ERROR, requestURI);
    }

    private ResponseEntity<Object> buildAndReturnErrorResponse(List<String> exMessage, HttpStatus status, String uri) {
        ErrorResponse errorResponse = new ErrorResponse(status.getReasonPhrase(), status.value(), exMessage, uri);
        return new ResponseEntity<>(errorResponse, new HttpHeaders(), status);
    }
}
