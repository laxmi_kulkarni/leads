package ai.lentra.leads.repository;

import ai.lentra.leads.model.Email;
import org.springframework.data.jpa.repository.JpaRepository;

public interface EmailRepository extends JpaRepository<Email, Long> {
}
