package ai.lentra.leads.repository;

import ai.lentra.leads.model.Contact;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.transaction.annotation.Transactional;


/**
 * @author laxmik@lentra.ai
 */
@RepositoryRestResource(exported = false)
public interface ContactRepository extends JpaRepository<Contact, Long> {

    @Modifying
    @Query(value = "insert into lead_contact (lead_id, contact_id) select :lead_id, :contact_id", nativeQuery = true)
    @Transactional(rollbackFor = Exception.class)
    void saveLeadContactAssociation(@Param("lead_id") Long leadId, @Param("contact_id") Long contactId);

    @Query(value = "select lead_id from lead_contact where lead_id = ?1 AND contact_id = ?2", nativeQuery = true)
    Long findLeadContactAssociation(Long leadId, Long contactId);

    /*@Query("{ $or : [{'firstName' : {$regex : ?0, $options :'i'}}," +
            "{'lastName' : {$regex : ?0, $options :'i'}} ] }")*/
    //List<Contact> findContactsBySearchTerm(String searchTerm);

}
