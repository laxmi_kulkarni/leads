package ai.lentra.leads.repository;

import ai.lentra.leads.model.Address;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AddressRepository  extends JpaRepository<Address, Long> {
}
