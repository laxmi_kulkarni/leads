package ai.lentra.leads.repository;

import ai.lentra.leads.model.Lead;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

/**
 * @author laxmik@lentra.ai
 */
@RepositoryRestResource(exported = false)
public interface LeadRepository extends JpaRepository<Lead, Long> {

}
