package ai.lentra.leads.commons;

import javax.validation.Constraint;
import java.lang.annotation.*;

/**
 * @author laxmik@lentra.ai
 */

@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy = {OperationValidator.class})
@Documented
public @interface RestrictOperation {
    String message() default "Operation not supported";

    Class<?>[] groups() default {};
}
