package ai.lentra.leads.commons;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

/**
 * @author laxmik@lentra.ai
 */
public class ErrorResponse extends APIResponse{

    @JsonProperty("status")
    private int status;
    @JsonProperty("error_details")
    private List<String> errorDetails;
    @JsonProperty("path")
    private String path;

    public ErrorResponse(String message, int status, List<String> errorDetails, String path) {
        super(message);
        this.status = status;
        this.errorDetails = errorDetails;
        this.path = path;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public List<String> getErrorDetails() {
        return errorDetails;
    }

    public void setErrorDetails(List<String> errorDetails) {
        this.errorDetails = errorDetails;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }
}
