package ai.lentra.leads.commons;

import org.hibernate.dialect.PostgreSQL94Dialect;

import java.sql.Types;

/**
 * @author laxmik@lentra.ai
 */
public class CustomPostgresSQLDialect extends PostgreSQL94Dialect {

    public CustomPostgresSQLDialect() {
        this.registerColumnType(Types.JAVA_OBJECT, "jsonb");
    }
}
