package ai.lentra.leads.commons;


import java.util.Objects;

/**
 * @author laxmik@lentra.ai
 */
public class CustomAttribute {
    private String serializedValue;
    private String attributeId;

    public String getSerializedValue() {
        return serializedValue;
    }

    public void setSerializedValue(String serializedValue) {
        this.serializedValue = serializedValue;
    }

    public String getAttributeId() {
        return attributeId;
    }

    public void setAttributeId(String attributeId) {
        this.attributeId = attributeId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CustomAttribute that = (CustomAttribute) o;
        return Objects.equals(serializedValue, that.serializedValue) && Objects.equals(attributeId, that.attributeId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(serializedValue, attributeId);
    }
}
