package ai.lentra.leads.commons;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * @author laxmik@lentra.ai
 */
@JsonInclude(JsonInclude.Include.NON_ABSENT)
public class SuccessResponse extends APIResponse {

    @JsonProperty("body")
    private Object responseBody;

    public SuccessResponse(String message, Object responseBody) {
        super(message);
        this.responseBody = responseBody;
    }

    public Object getResponseBody() {
        return responseBody;
    }

    public void setResponseBody(Object responseBody) {
        this.responseBody = responseBody;
    }
}
