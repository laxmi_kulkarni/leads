package ai.lentra.leads.commons;

/**
 * @author laxmik@lentra.ai
 */
public class ErrorMessage {

    public static final String LEAD_NOT_FOUND = "Lead/s not found";
    public static final String UPDATE_CONTACT_NOT_SUPPORTED = "PATCH /leads api does not support updating contacts";

    public static final String CONTACT_NOT_FOUND = "Contact/s not found";
    public static final String ADDRESS_NOT_FOUND = "Address/s not found";
    public static final String PHONE_DETAILS_NOT_FOUND = "Phone details not found";
    public static final String EMAIL_NOT_FOUND = "Email/s not found";
    public static final String CONTACT_NOT_FOUND_IN_LEAD = "Contact not found in given Lead details.";
    public static final String ADDRESS_NOT_FOUND_IN_CONTACT = "Address not found in given Contact details.";
    public static final String PHONE_DETAILS_NOT_FOUND_IN_CONTACT = "Phone details not found in given Contact details.";
    public static final String EMAIL_NOT_FOUND_IN_CONTACT = "Email not found in given Contact details.";
}
