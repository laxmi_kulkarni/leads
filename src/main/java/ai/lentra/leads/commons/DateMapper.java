package ai.lentra.leads.commons;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

/**
 * @author laxmik@lentra.ai
 */
public class DateMapper {

    DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");

    public String asString(LocalDate date) {
        return date != null ? formatter.format(date) : null;
    }

    public LocalDate asDate(String date) {
        return date != null ? LocalDate.parse(date, formatter) : null;
    }
}
