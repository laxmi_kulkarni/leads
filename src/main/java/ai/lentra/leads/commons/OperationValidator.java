package ai.lentra.leads.commons;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

/**
 * @author laxmik@lentra.ai
 */

public class OperationValidator implements ConstraintValidator<RestrictOperation, Object> {

    @Override
    public boolean isValid(Object o, ConstraintValidatorContext constraintValidatorContext) {
        return o == null;
    }
}
