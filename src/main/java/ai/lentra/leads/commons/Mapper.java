package ai.lentra.leads.commons;

import ai.lentra.leads.dto.*;
import ai.lentra.leads.model.*;

/**
 * @author laxmik@lentra.ai
 */
@org.mapstruct.Mapper(uses = {DateMapper.class})
public interface Mapper {

    LeadRequestDTO leadEntityToDto(Lead lead);
    Lead leadDtoToEntity(LeadRequestDTO leadRequestDTO);

    ContactDTO contactEntityToDto(Contact contact);
    Contact contactDtoToEntity(ContactDTO contactDTO);

    AddressDTO addressEntityToDto(Address address);
    Address addressDtoToEntity(AddressDTO addressDTO);

    PhoneDTO phoneEntityToDto(Phone phone);
    Phone phoneDtoToEntity(PhoneDTO phoneDTO);

    EmailDTO emailEntityToDto(Email email);
    Email emailDtoToEntity(EmailDTO emailDTO);

/*
    @BeanMapping(nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE)
    void updateLeadEntity(Lead updatedLead, @MappingTarget Lead existingLead);*/

}
