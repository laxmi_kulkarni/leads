package ai.lentra.leads.commons;

public enum DataType {
    NUMBER,
    STRING,
    DATE_TIME,
    REFERENCE
}
