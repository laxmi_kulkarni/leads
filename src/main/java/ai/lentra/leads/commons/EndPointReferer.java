package ai.lentra.leads.commons;

public class EndPointReferer {

    public static final String LEADS = "/leads";
    public static final String LEAD_ID = "/{leadId}";
    public static final String CREATE_LEAD = "/create-lead";
    public static final String UPDATE_LEAD = "/update-lead";

    public static final String CONTACTS = "/contacts";
    public static final String CONTACT_ID = "/{contactId}";
    public static final String CREATE_CONTACT = "/create-contact";
    public static final String UPDATE_CONTACT = "/update-contact";

    public static final String ADDRESS = "/address";
    public static final String ADDRESS_ID = "/{addressId}";
    public static final String CREATE_ADDRESS = "/create-address";
    public static final String UPDATE_ADDRESS = "/update-address";

    public static final String PHONES = "/phones";
    public static final String PHONE_ID = "/{phoneId}";
    public static final String CREATE_PHONE = "/create-phone";
    public static final String UPDATE_PHONE = "/update-phone";

    public static final String EMAILS = "/emails";
    public static final String EMAIL_ID = "/{emailId}";
    public static final String CREATE_EMAIL = "/create-email";
    public static final String UPDATE_EMAIL = "/update-email";
}
