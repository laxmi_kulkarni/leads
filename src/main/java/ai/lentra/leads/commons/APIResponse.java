package ai.lentra.leads.commons;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * @author laxmik@lentra.ai
 */
public class APIResponse {
    @JsonProperty("message")
    private String message;

    public APIResponse(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
