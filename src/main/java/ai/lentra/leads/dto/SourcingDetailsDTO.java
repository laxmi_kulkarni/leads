package ai.lentra.leads.dto;

import ai.lentra.leads.commons.CreateGrp;
import ai.lentra.leads.commons.CustomAttribute;
import com.fasterxml.jackson.annotation.JsonProperty;

import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * @author laxmik@lentra.ai
 */
public class SourcingDetailsDTO {

    @JsonProperty("id")
    private Long id;
    @JsonProperty("channel_type")
    private String channelType;
    @JsonProperty("channel_id")
    private Integer channelId;
    @JsonProperty("medium")
    private String medium;
    @JsonProperty("user_id")
    @NotNull(groups = CreateGrp.class, message = "Provide Sourcing User Id.")
    private Integer userId;
    @JsonProperty("location")
    private String location;
    @JsonProperty("branch")
    private String branch;
    @JsonProperty("custom_attributes")
    private List<CustomAttribute> customAttributes;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getChannelType() {
        return channelType;
    }

    public void setChannelType(String channelType) {
        this.channelType = channelType;
    }

    public Integer getChannelId() {
        return channelId;
    }

    public void setChannelId(Integer channelId) {
        this.channelId = channelId;
    }

    public String getMedium() {
        return medium;
    }

    public void setMedium(String medium) {
        this.medium = medium;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getBranch() {
        return branch;
    }

    public void setBranch(String branch) {
        this.branch = branch;
    }

    public List<CustomAttribute> getCustomAttributes() {
        return customAttributes;
    }

    public void setCustomAttributes(List<CustomAttribute> customAttributes) {
        this.customAttributes = customAttributes;
    }
}
