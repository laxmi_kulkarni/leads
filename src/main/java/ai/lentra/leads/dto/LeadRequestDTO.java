package ai.lentra.leads.dto;


import ai.lentra.leads.commons.CreateGrp;
import ai.lentra.leads.commons.CustomAttribute;
import ai.lentra.leads.commons.UpdateGrp;
import com.fasterxml.jackson.annotation.JsonProperty;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Null;
import java.util.List;

import static ai.lentra.leads.commons.ErrorMessage.UPDATE_CONTACT_NOT_SUPPORTED;

/**
 * @author laxmik@lentra.ai
 */

public class LeadRequestDTO {

    @JsonProperty("id")
    private Long id;
    @JsonProperty("institution_id")
    private String institutionId;
    @JsonProperty("current_stage")
    private String currentStage;
    @JsonProperty("current_status")
    private String currentStatus;
    @JsonProperty("external_id")
    private String externalId;
    @JsonProperty("external_id_type")
    private Integer externalIdType;
    @JsonProperty("created_date")
    private String createdOn;
    @JsonProperty("follow_up_date")
    private String followUpDate;
    @JsonProperty("contacts")
    @Null(groups = {UpdateGrp.class}, message = UPDATE_CONTACT_NOT_SUPPORTED)
    private List<ContactDTO> contacts;
    @JsonProperty("leads_activity")
    private List<LeadsActivityDTO> leadsActivities;
    @JsonProperty("requested_loan_details")
    private RequestedLoanDetailsDTO requestedLoanDetails;
    @JsonProperty("sourcing_details")
    @NotNull(groups = CreateGrp.class, message = "Provide sourcing details.")
    @Valid
    private SourcingDetailsDTO sourcingDetails;
    @JsonProperty("custom_attributes")
    private List<CustomAttribute> customAttributes;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getInstitutionId() {
        return institutionId;
    }

    public void setInstitutionId(String institutionId) {
        this.institutionId = institutionId;
    }

    public String getCurrentStage() {
        return currentStage;
    }

    public void setCurrentStage(String currentStage) {
        this.currentStage = currentStage;
    }

    public String getCurrentStatus() {
        return currentStatus;
    }

    public void setCurrentStatus(String currentStatus) {
        this.currentStatus = currentStatus;
    }

    public String getExternalId() {
        return externalId;
    }

    public void setExternalId(String externalId) {
        this.externalId = externalId;
    }

    public Integer getExternalIdType() {
        return externalIdType;
    }

    public void setExternalIdType(Integer externalIdType) {
        this.externalIdType = externalIdType;
    }

    public String getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(String createdOn) {
        this.createdOn = createdOn;
    }

    public String getFollowUpDate() {
        return followUpDate;
    }

    public void setFollowUpDate(String followUpDate) {
        this.followUpDate = followUpDate;
    }

    public List<ContactDTO> getContacts() {
        return contacts;
    }

    public void setContacts(List<ContactDTO> contacts) {
        this.contacts = contacts;
    }

    public List<LeadsActivityDTO> getLeadsActivities() {
        return leadsActivities;
    }

    public void setLeadsActivities(List<LeadsActivityDTO> leadsActivities) {
        this.leadsActivities = leadsActivities;
    }

    public RequestedLoanDetailsDTO getRequestedLoanDetails() {
        return requestedLoanDetails;
    }

    public void setRequestedLoanDetails(RequestedLoanDetailsDTO requestedLoanDetails) {
        this.requestedLoanDetails = requestedLoanDetails;
    }

    public SourcingDetailsDTO getSourcingDetails() {
        return sourcingDetails;
    }

    public void setSourcingDetails(SourcingDetailsDTO sourcingDetails) {
        this.sourcingDetails = sourcingDetails;
    }

    public List<CustomAttribute> getCustomAttributes() {
        return customAttributes;
    }

    public void setCustomAttributes(List<CustomAttribute> customAttributes) {
        this.customAttributes = customAttributes;
    }
}
