package ai.lentra.leads.dto;

import ai.lentra.leads.commons.UpdateGrp;
import com.fasterxml.jackson.annotation.JsonProperty;

import javax.validation.constraints.NotNull;

/**
 * @author laxmik@lentra.ai
 */
public class LeadsActivityDTO {
    @JsonProperty("id")
    @NotNull(groups = {UpdateGrp.class})
    private Long id;
    @JsonProperty("data")
    private String data;
    @JsonProperty("type")
    private String type;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
