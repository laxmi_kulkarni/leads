package ai.lentra.leads.dto;

import ai.lentra.leads.commons.CustomAttribute;
import ai.lentra.leads.commons.UpdateGrp;
import ai.lentra.leads.model.EmailVerificationType;
import com.fasterxml.jackson.annotation.JsonProperty;

import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * @author laxmik@lentra.ai
 */
public class EmailDTO {

    @JsonProperty("id")
    private Long id;
    @JsonProperty("email_id")
    private String emailId;
    @JsonProperty("email_of_guarantor")
    private String emailIdOfGuarantor;
    @JsonProperty("email_verification_type")
    private EmailVerificationType emailVerificationType;
    @JsonProperty("custom_attributes")
    private List<CustomAttribute> customAttributes;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getEmailId() {
        return emailId;
    }

    public void setEmailId(String emailId) {
        this.emailId = emailId;
    }

    public String getEmailIdOfGuarantor() {
        return emailIdOfGuarantor;
    }

    public void setEmailIdOfGuarantor(String emailIdOfGuarantor) {
        this.emailIdOfGuarantor = emailIdOfGuarantor;
    }

    public EmailVerificationType getEmailVerificationType() {
        return emailVerificationType;
    }

    public void setEmailVerificationType(EmailVerificationType emailVerificationType) {
        this.emailVerificationType = emailVerificationType;
    }

    public List<CustomAttribute> getCustomAttributes() {
        return customAttributes;
    }

    public void setCustomAttributes(List<CustomAttribute> customAttributes) {
        this.customAttributes = customAttributes;
    }
}
