package ai.lentra.leads.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.math.BigDecimal;

/**
 * @author laxmik@lentra.ai
 */
public class RequestedLoanDetailsDTO {

    @JsonProperty("id")
    private Long id;
    @JsonProperty("loan_amount")
    private BigDecimal amount;
    @JsonProperty("payment")
    private BigDecimal payment; //emi
    @JsonProperty("payment_frequency")
    private String paymentFrequency;
    // tenure associated with tenure unit. e.g. if (tenure = 12 and tenure_unit = "months")
    // then loan tenure is 12 months
    @JsonProperty("tenure")
    private Integer tenure;
    @JsonProperty("tenure_unit")
    private String tenureUnit; //e.g. "months"

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public BigDecimal getPayment() {
        return payment;
    }

    public void setPayment(BigDecimal payment) {
        this.payment = payment;
    }

    public String getPaymentFrequency() {
        return paymentFrequency;
    }

    public void setPaymentFrequency(String paymentFrequency) {
        this.paymentFrequency = paymentFrequency;
    }

    public Integer getTenure() {
        return tenure;
    }

    public void setTenure(Integer tenure) {
        this.tenure = tenure;
    }

    public String getTenureUnit() {
        return tenureUnit;
    }

    public void setTenureUnit(String tenureUnit) {
        this.tenureUnit = tenureUnit;
    }
}
