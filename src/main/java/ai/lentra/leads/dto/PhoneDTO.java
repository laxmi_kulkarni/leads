package ai.lentra.leads.dto;

import ai.lentra.leads.commons.CustomAttribute;
import ai.lentra.leads.commons.UpdateGrp;
import ai.lentra.leads.model.CommunicationLocationType;
import ai.lentra.leads.model.CommunicationType;
import ai.lentra.leads.model.CommunicationVerificationType;
import com.fasterxml.jackson.annotation.JsonProperty;

import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * @author laxmik@lentra.ai
 */
public class PhoneDTO {

    @JsonProperty("id")
    private Long id;
    @JsonProperty("phone_number")
    private String phoneNumber;
    @JsonProperty("country_code")
    private Integer countryCode;
    @JsonProperty("province_code")
    private Integer provinceCode;
    @JsonProperty("location_type")
    private CommunicationLocationType communicationLocationType;
    @JsonProperty("communication_type")
    private CommunicationType communicationType;
    @JsonProperty("verification_type")
    private CommunicationVerificationType communicationVerificationType;
    @JsonProperty("alternate_number")
    private String alternatePhoneNumer;
    @JsonProperty("custom_attributes")
    private List<CustomAttribute> customAttributes;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public Integer getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(Integer countryCode) {
        this.countryCode = countryCode;
    }

    public Integer getProvinceCode() {
        return provinceCode;
    }

    public void setProvinceCode(Integer provinceCode) {
        this.provinceCode = provinceCode;
    }

    public CommunicationLocationType getCommunicationLocationType() {
        return communicationLocationType;
    }

    public void setCommunicationLocationType(CommunicationLocationType communicationLocationType) {
        this.communicationLocationType = communicationLocationType;
    }

    public CommunicationType getCommunicationType() {
        return communicationType;
    }

    public void setCommunicationType(CommunicationType communicationType) {
        this.communicationType = communicationType;
    }

    public CommunicationVerificationType getCommunicationVerification() {
        return communicationVerificationType;
    }

    public void setCommunicationVerification(CommunicationVerificationType communicationVerificationType) {
        this.communicationVerificationType = communicationVerificationType;
    }

    public String getAlternatePhoneNumer() {
        return alternatePhoneNumer;
    }

    public void setAlternatePhoneNumer(String alternatePhoneNumer) {
        this.alternatePhoneNumer = alternatePhoneNumer;
    }

    public List<CustomAttribute> getCustomAttributes() {
        return customAttributes;
    }

    public void setCustomAttributes(List<CustomAttribute> customAttributes) {
        this.customAttributes = customAttributes;
    }
}
