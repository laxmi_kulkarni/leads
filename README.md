## Repository for the Lead management

The service is a Spring boot based project which uses Postgres as Database.
The project includes Docker Compose files for deploying and running the full setup.

---

## Required installations for developers
1. JDK 11
2. Maven
3. Docker Engine
4. Docker Compose

---

## Running the project

Build the project with `mvn clean package` or with `mvn clean package -DskipTests` to skip tests.

There are 2 options to run the project

**Running with only a DB for minimal Dev setup**

From inside the `deployment-scripts/db` folder just run `sudo docker-compose up --build` to bring up the Database

Then either run the project through IDE or from project root execute `java -jar ./target/prototype-be-0.0.1-SNAPSHOT.jar` to execute jar built by mvn.
    The service should automatically connect to, initialize and use the DB from step 1.
       
**Running with full production setup**

From inside the `deployment-scripts/prod` folder run `sudo bash run-with-jar.sh`. Ensure the script has execute permissions if needed with `chmod u+x run-with-jar.sh`.

This will start the following -
1. Spring boot service (http://localhost:8080)
2. Postgres DB
3. FluentD
4. Elasticsearch
5. Kibana (http://localhost:5601)
6. Prometheus (http://localhost:9090)
7. Grafana (http://localhost:3000)

#Swagger UI will be available at
http://localhost:<port>/api/swagger-ui/index.html?configUrl=/api/docs/swagger-config#/

---

## Frontend 
Todo